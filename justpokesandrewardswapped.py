import RPi.GPIO as GPIO
import time
import multimaestro
import os
import csv
import numpy as np
import random

##UPDATED 1.18.22 to record poke length at .25s interval
#Updated 10.27.22 to swap red and white syringe pumps

###for just pokes session--- unplug all the IR beams :)
###ok v confusing need to disable door IRs somehow but just unplugging them does not work and makes them close at random
###not allowing them to close also sometimes trips up so not really an option
#my variables uwu
#timings in seconds
T_trial= time.time() +60
minute = 60
T_sesh=time.time() + 60*minute
#pi pins
IR_0 =17  
IR_1 =22 
IR_2 =6 
IR_3 =19 #ithink this is extra
#add whichever is the camera ... later

#polo pins
water0 =0
water1 =1
water2 =2
water3 =3 #ithink this is extra

door0 =4 
door1 =5
door2 =6
door3 =7
door4 =8

led0 =9
led1 =10
led2 =11
led3 =12

odor1 =13 #0,1,2 in JM notes
odor2 =14
odor3 =15
bank1 =16
bank2 =17
bank3 =18
odortrig =19

odorout =20
waterout =21
ledout =22
doorout =23

#set up
polo = multimaestro.Controller()
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup([IR_0,IR_1,IR_2,IR_3], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(16,GPIO.OUT) #trigger cam

polo.setSpeed(water0, 10)
polo.setSpeed(water1, 10)
polo.setSpeed(water2, 10)
polo.setSpeed(water3, 10)
polo.setAccel(water0, 4)
polo.setAccel(water1, 4)
polo.setAccel(water2, 4)
polo.setAccel(water3, 4)
start = polo.getPosition(0)

on = 7000
off = 0
opened = 0
canclose = 7000
closed = 7000


now = time.time()
datetime = str(time.strftime("%d.%b.%y_%H.%M"))

repeat0= 0
repeat1= 0
repeat2= 0
recent= [7,7,7]
re0,re1,re2,poke0,poke1,poke2= 0,1,2,3,4,5

def odorxbankx(odorx,bankx):
    polo.setTarget(odor3, off)
    polo.setTarget(odorx, on)
    polo.setTarget(bankx, on)
    polo.setTarget(odorout, on)
    polo.setTarget(odortrig, on)
    time.sleep(2)
    polo.setTarget(odorx, off)
    polo.setTarget(bankx, off)
    polo.setTarget(odor3, on)
    polo.setTarget(odorout, off)
    polo.setTarget(odortrig, off)
    
def opendoor(doorx):
    polo.setTarget(doorx, opened)
    polo.setTarget(doorout, on)
    time.sleep(.1)
    polo.setTarget(doorx, canclose)
    polo.setTarget(doorout, off)
    
def openalldoors():
    opendoor(door0)
    opendoor(door1)
    opendoor(door2)
    opendoor(door3)
    opendoor(door4)

def allled():
    polo.setTarget(led0,on)
    polo.setTarget(led1,on) 
    polo.setTarget(led2,on)
    polo.setTarget(led3,on)
    polo.setTarget(ledout,on)
    time.sleep(.5)
    polo.setTarget(ledout,off)
    
def allledoff():
    polo.setTarget(led0,off)
    polo.setTarget(led1,off) 
    polo.setTarget(led2,off)
    polo.setTarget(led3,off)
    polo.setTarget(ledout,off)
def reward(waterx):
    polo.setTarget(waterx, opened)
    polo.setTarget(waterout, on)
    time.sleep(1)#seconds of water pumping
    polo.setTarget(waterx, canclose)
    polo.setTarget(waterout, off)
def changes(reX,x):
    listo=[0,0,0,0,0,0]
    listo[reX]=1
    listo[x]=1
    now=time.time()
    file.write(str(now)+","+str(listo[0])+","+str(listo[1])+","+str(listo[2])+","+str(listo[3])+","+str(listo[4])+","+str(listo[5])+"\n")

   
  
allled()
openalldoors()
GPIO.output(16, GPIO.HIGH)
time.sleep(0.75)
GPIO.output(16,GPIO.LOW)
    
bb= "/home/pi/Desktop/"+ datetime +".csv"
with open("/home/pi/Desktop/"+ datetime +".csv", "a")as file:
    file.write("Time,re0,re1,re2,poke0,poke1,poke2"+ "\n")
    file.write(str(now)+",0,0,0,0,0,0"+ "\n")
    while time.time() < T_sesh:
        if (GPIO.input(IR_0)== False and repeat0 == 0):
            repeat0 = 1
            recent.append(1)
            recent = recent[-3:]
            reward(water0)
            polo.setTarget(led0,off)
            now = time.time()
            changes(re0,poke0)
            print("poke at IR_0") #this if gives reward if IR_0 is poked, and then prevents it from being repeatedly pookked
            
        if (GPIO.input(IR_1)== False and repeat1 ==0):
            repeat1 = 1
            recent.append(2)
            recent = recent[-3:]
            reward(water1)
            polo.setTarget(led1,off)
            now = time.time()
            changes(re1,poke1)
            print("poke at IR_1") #same as if above for IR_1
            
        if (GPIO.input(IR_2)== False and repeat2 ==0):
            repeat2 = 1
            recent.append(3)
            recent = recent[-3:]
            reward(water2)
            polo.setTarget(led2,off)
            now = time.time()
            changes(re2,poke2)
            print("poke at IR_2") #same as if above for IR_1
            
        if recent[1] == 1: #resets status of pokes when another port is poked IR_0
            repeat0=0
            polo.setTarget(led0,on)
            time.sleep(.25)
        if recent[1] == 2: #reset for IR_1
            repeat1=0
            polo.setTarget(led1,on)
            time.sleep(.25)
        if recent[1] == 3: #reset for IR_2
            repeat2=0
            polo.setTarget(led2,on)
            time.sleep(.25)
            
        if (GPIO.input(IR_0)== False and repeat0 == 1):
            changes(poke0,poke0)
            time.sleep(.25)
            print("repeat 0")
            
        if (GPIO.input(IR_1)== False and repeat1 == 1):
            changes(poke1,poke1)
            time.sleep(.25)
            print("repeat 1")
            
        if (GPIO.input(IR_2)== False and repeat2 == 1):
            changes(poke2,poke2)
            time.sleep(.25)
            print("repeat 2")
            
file.close()       
allledoff()
GPIO.cleanup()    
        
mouseid= input('input mouse id: ') +'_'
trainday= input('input training day: ') +'_'          
        
os.rename(bb, "/home/pi/Desktop/"+ str(mouseid) + str(trainday) + str(datetime) + ".csv")   


###make this script work properly... current vibe is to unplug servos at their end :) email john incase theres a better work around 
###no odor-- 1st. homecage poke, Lreward home random L and R
###2 trial-- L not rward, not poke needed--- both doors open== then opposite to get reward,, wrong doors close behind and open to finish no reward
######>>>random LR
    