#imports :)
import RPi.GPIO as GPIO
import time
import multimaestro
import os
import csv
import numpy as np
import random


#my variables uwu
#pi pins
IR_0 =17  
IR_1 =22 
IR_2 =6 
IR_3 =19 #ithink this is extra
#add whichever is the camera ... later

#polo pins
water0 =0
water1 =1
water2 =2
water3 =3 #ithink this is extra

door0 =4 
door1 =5
door2 =6
door3 =7
door4 =8

led0 =9
led1 =10
led2 =11
led3 =12

odor1 =13 #0,1,2 in JM notes
odor2 =14
odor3 =15
bank1 =16
bank2 =17
bank3 =18
odortrig =19

odorout =20
waterout =21
ledout =22
doorout =23

#set up
polo = multimaestro.Controller()
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup([IR_0,IR_1,IR_2,IR_3], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(16,GPIO.OUT) #trigger cam

polo.setSpeed(water0, 10)
polo.setSpeed(water1, 10)
polo.setSpeed(water2, 10)
polo.setSpeed(water3, 10)
polo.setAccel(water0, 4)
polo.setAccel(water1, 4)
polo.setAccel(water2, 4)
polo.setAccel(water3, 4)
start = polo.getPosition(0)

on = 7000
off = 0
opened = 0
canclose = 7000
#timings in seconds
T_trial= time.time() +60
minute = 60
T_sesh=time.time() + minute


odorA_Left =(odor1,door4,IR_2,bank1,water2,door2)
odorB_Left = (odor2,door4,IR_2,bank1,water2,door2)
odorA_Right = (odor1,door3,IR_1,bank1,water1,door1)
odorB_Right = (odor2,door3,IR_1,bank1,water1,door1)


def odorxbankx(odorx,bankx):
    polo.setTarget(odor3, off)
    polo.setTarget(odorx, on)
    polo.setTarget(bankx, on)
    polo.setTarget(odorout, on)
    polo.setTarget(odortrig, on)
    time.sleep(2)
    polo.setTarget(odorx, off)
    polo.setTarget(bankx, off)
    polo.setTarget(odor3, on)
    polo.setTarget(odorout, off)
    polo.setTarget(odortrig, off)
    
def opendoor(doorx):
    polo.setTarget(doorx, opened)
    polo.setTarget(doorout, on)
    time.sleep(.1)
    polo.setTarget(doorx, canclose)
    polo.setTarget(doorout, off)
    
def open2doors(doorx,doory):
    polo.setTarget(doorx, opened)
    polo.setTarget(doory, opened)
    polo.setTarget(doorout, on)
    time.sleep(.1)
    polo.setTarget(doorx, canclose)
    polo.setTarget(doory, canclose)
    polo.setTarget(doorout, off)
    
def reward(waterx):
    polo.setTarget(waterx, opened)
    polo.setTarget(waterout, on)
    time.sleep(1)
    polo.setTarget(waterx, canclose)
    polo.setTarget(waterout, off)
    

    
def odortoreward(odorx,choice,IR_X,bankx,rewardx,doorx,IR_Y,doory):
    while time.time() < T_trial:
        if GPIO.input(IR_0) == False:
            open2doors(door3,door4)
            print("poke at 0")
            polo.setTarget(led0,off)
            odorxbankx(odorx,bank1)
            time.sleep(2)
            opendoor(door0)
            time.sleep(2)
            #opendoor(choice)
        if GPIO.input(IR_X) == False:
            print(f"poke at {IR_X}")
            #odorxbankx(odorx,bankx)
            reward(rewardx)
            opendoor(doorx)
        if GPIO.input(IR_Y) == False:
            print(f"poke at {IR_X}")
            opendoor(doory)
            
        
def nonetoodor(odorx,IR_X,bankx,rewardx,doorx,doory):
    while time.time() < T_trial:
        if GPIO.input(IR_0) == False:
            opendoor(doorx) #the L or R door
            print("poke at 0")
            polo.setTarget(led0,off)
            #odorxbankx(odorx,bank1)
            time.sleep(2)
            opendoor(door0)
        if GPIO.input(IR_X) == False:
            print(f"poke at {IR_X}")
            odorxbankx(odorx,bankx)
            #reward(rewardx)
            opendoor(doory)#the final door
        #set to 60 sec minimum
            

def odorA_Leftp1():
    nonetoodor(odor1,IR_2,bank3,water2,door4,door2)
def odorA_Leftp2():
    odortoreward(odor1,door4,IR_2,bank3,water2,door2,IR_1,door1)
def odorB_Left():
    odortoreward(odor2,door4,IR_2,bank3,water2,door2,IR_1,door1)
def odorA_Right():
    odortoreward(odor1,door3,IR_1,bank3,water1,door1,IR_2,door4)
def odorB_Right():
    odortoreward(odor2,door3,IR_1,bank3,water1,door1,IR_2,door4)
            
polo.setTarget(led0,on)

#trigger cam
GPIO.output(16, GPIO.HIGH)
time.sleep(0.75)
GPIO.output(16,GPIO.LOW)
    
while time.time() < T_sesh: 
    #led > poke > odorA,B,Reward for habituation to ports
    #trialtype(odor1,door4,IR_2,bank1,water2,door2)
    #odorA_Leftp1()
    #polo.setTarget(led0,on)
    odorA_Leftp2()
    #odorB_Left()
    #odorA_Right()
    #odorB_Right()
    #reward(water2)
    
    
    #>openall doors, reward at every port 
